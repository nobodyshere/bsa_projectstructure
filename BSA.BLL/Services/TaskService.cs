﻿using BSA.BLL.DTOs.LinqDTOs;
using BSA.BLL.Services.Interfaces;
using BSA.DataAccess.Entities;
using BSA.DataAccess.UnityOfWork;
using System.Collections.Generic;
using System.Linq;

namespace BSA.BLL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TaskService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<UserTasksCountDTO> GetNumberOfUserTasks(int userId)
        {
            return _unitOfWork.Tasks.GetAll()
                .GroupBy(x => x.ProjectId)
                .Join(_unitOfWork.Projects.GetAll(), x => x.Key, y => y.Id, (x, y) => new
                {
                    Project = y,
                    TaskCount = x.Count()
                })
                .Where(x => x.Project.AuthorId == userId)
                .Select((x) => new UserTasksCountDTO()
                {
                    Project = x.Project,
                    TaskCount = x.TaskCount
                });
        }

        public IEnumerable<Task> GetListOfUserTasks(int userId)
        {
            return _unitOfWork.Tasks.GetAll()
                .Where(x => x.PerformerId == userId)
                .Where(y => y.Name.Length < 45);
        }

        public Dictionary<int, string> GetFinishedTasks(int userId)
        {
            return _unitOfWork.Tasks.GetAll()
                .Where(x => x.PerformerId == userId
                            && x.State == DataAccess.Entities.TaskState.Finished
                            && x.FinishedAt.Year == 2019)
                .ToDictionary(k => k.Id, v => v.Name);
        }
    }
}
