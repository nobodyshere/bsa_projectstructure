﻿using System.Linq;
using BSA.BLL.DTOs;
using BSA.BLL.Services.Interfaces;
using BSA.DataAccess.Entities;
using BSA.DataAccess.UnityOfWork;

namespace BSA.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProjectService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ProjectTasksInfoDTO GetProjectTasksInfo(int projectId)
        {
            return (from project in _unitOfWork.Projects.GetAll()
                    where project.Id == projectId
                join task in _unitOfWork.Tasks.GetAll() on project.Id equals task.ProjectId
                    into taskList
                let longestTask = (from task in _unitOfWork.Tasks.GetAll()
                                   where task.ProjectId == projectId
                    orderby task.Description descending
                    select task).FirstOrDefault()
                let shortestTask = (from task in _unitOfWork.Tasks.GetAll()
                                    where task.ProjectId == projectId
                    orderby task.Name
                    select task).FirstOrDefault()
                let users = from user in _unitOfWork.Users.GetAll()
                    where user.TeamId == project.TeamId && (project.Description.Length > 25 || taskList.Count() < 3)
                    select user
                select new ProjectTasksInfoDTO() {
                    project = project,
                    longestTask = longestTask,
                    shortestTask = shortestTask,
                    usersCount = users.Count()
                    }).FirstOrDefault();
        }
    }
}
