﻿using BSA.BLL.DTOs;
using BSA.BLL.Services.Interfaces;
using BSA.DataAccess.UnityOfWork;
using System.Collections.Generic;
using System.Linq;

namespace BSA.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<SortedUsersDTO> GetSortedUsers()
        {
            return _unitOfWork.Users.GetAll().OrderBy(x => x.FirstName).Select((x) => new SortedUsersDTO
            {
                user = x,
                tasks = _unitOfWork.Tasks.GetAll()
                    .Where(y => y.PerformerId == x.Id)
                    .OrderByDescending(z => z.Name.Length).ToList()
            });
        }

        public UserTasksDataDTO GetUserTasksData(int userId)
        {
            var currentUser = _unitOfWork.Users.GetAll().FirstOrDefault(x => x.Id == userId);
            var lastProject = _unitOfWork.Projects.GetAll()
                .Where(x => x.AuthorId == userId)
                .OrderByDescending(x => x.CreatedAt)
                .FirstOrDefault();
            var tasksCount = _unitOfWork.Tasks.GetAll().GroupBy(x => x.ProjectId)
                .FirstOrDefault(x => x.Key == lastProject.Id).Count();
            var notFinishedTask = _unitOfWork.Tasks.GetAll().Count(x => x.ProjectId == lastProject.Id && x.State != DataAccess.Entities.TaskState.Finished);
            var longestTaskByDate = _unitOfWork.Tasks.GetAll().Where(x => x.PerformerId == userId).OrderBy(x => x.CreatedAt)
                .ThenByDescending(x => x.FinishedAt)
                .FirstOrDefault();

            return new UserTasksDataDTO()
            {
                user = currentUser,
                lastProject = lastProject,
                lastProjectTasksCount = tasksCount,
                longestTaskByDate = longestTaskByDate,
                unfinishedTasksCount = notFinishedTask
            };
        }
    }
}