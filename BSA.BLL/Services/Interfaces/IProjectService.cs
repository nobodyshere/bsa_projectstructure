﻿using BSA.BLL.DTOs;

namespace BSA.BLL.Services.Interfaces
{
    public interface IProjectService
    {
        ProjectTasksInfoDTO GetProjectTasksInfo(int projectId);
    }
}