﻿using System.Collections.Generic;
using BSA.BLL.DTOs;

namespace BSA.BLL.Services.Interfaces
{
    public interface ITeamService
    {
        IEnumerable<TeamWithOlderUsersDTO> GetListOfOlderUsers();
    }
}