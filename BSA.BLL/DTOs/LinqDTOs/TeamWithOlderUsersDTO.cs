﻿using System;
using System.Collections.Generic;
using System.Text;
using BSA.DataAccess.Entities;

namespace BSA.BLL.DTOs
{
    public class TeamWithOlderUsersDTO
    {
        public int teamId;
        public string teamName;
        public List<User> users;
    }
}
