﻿using System;
using System.Collections.Generic;
using System.Text;
using BSA.DataAccess.Entities;

namespace BSA.BLL.DTOs
{
    public class UserTasksDataDTO
    {
        public User user;
        public Project lastProject;
        public int lastProjectTasksCount;
        public int unfinishedTasksCount;
        public Task longestTaskByDate;
    }
}
