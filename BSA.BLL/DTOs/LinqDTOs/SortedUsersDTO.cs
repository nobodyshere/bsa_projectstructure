﻿using System;
using System.Collections.Generic;
using System.Text;
using BSA.DataAccess.Entities;

namespace BSA.BLL.DTOs
{
    public class SortedUsersDTO
    {
        public User user;
        public List<Task> tasks;
    }
}
