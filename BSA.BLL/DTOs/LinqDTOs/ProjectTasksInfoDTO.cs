﻿using BSA.DataAccess.Entities;

namespace BSA.BLL.DTOs
{
    public class ProjectTasksInfoDTO
    {
        public Project project;
        public Task longestTask;
        public Task shortestTask;
        public int usersCount;
    }
}
