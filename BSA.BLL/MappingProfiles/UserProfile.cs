﻿using AutoMapper;
using BSA.BLL.DTOs;
using BSA.DataAccess.Entities;


namespace BSA.BLL.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>();
        }
    }
}