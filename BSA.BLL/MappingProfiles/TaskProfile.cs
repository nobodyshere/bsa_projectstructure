﻿using AutoMapper;
using BSA.BLL.DTOs;
using BSA.DataAccess.Entities;

namespace BSA.BLL.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<TaskDTO, Task>();
            CreateMap<Task, TaskDTO>();
        }
    }
}
