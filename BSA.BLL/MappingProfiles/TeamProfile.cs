﻿using AutoMapper;
using BSA.BLL.DTOs;
using BSA.DataAccess.Entities;

namespace BSA.BLL.MappingProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<TeamDTO, Team>();
            CreateMap<Team, TeamDTO>();
        }
    }
}
