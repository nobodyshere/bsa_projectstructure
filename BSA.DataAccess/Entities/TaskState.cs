﻿namespace BSA.DataAccess.Entities
{
    [System.Serializable]
    public enum TaskState
    {
        Created = 0,
        Started,
        Finished,
        Cancelled
    }
}
