﻿using System;
using BSA.DataAccess.Entities;
using System.Collections.Generic;
using System.Linq;


namespace BSA.DataAccess.Repository
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly List<Team> _teams = new List<Team>();

        public Team Get(int id)
        {
            return _teams.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Team> GetAll()
        {
            return _teams;
        }

        public void Create(Team item)
        {
            if (_teams.Any())
            {
                var x = _teams.Last();
                item.Id = x.Id + 1;
                _teams.Add(item);
            }
            else
            {
                item.Id = 1;
                _teams.Add(item);
            }
        }

        public void Update(Team item)
        {
            var team = _teams.FirstOrDefault(x => x.Id == item.Id);
            if (team != null)
            {
                var index = _teams.FindIndex(x => x.Id == item.Id);
                _teams[index] = item;
            };
        }

        public void Delete(int id)
        {
            _teams.RemoveAt(_teams.FindIndex(x => x.Id == id));
        }
    }
}
