﻿using System;
using BSA.DataAccess.Entities;
using System.Collections.Generic;
using System.Linq;


namespace BSA.DataAccess.Repository
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly List<Project> _projects = new List<Project>();

        public void Create(Project item)
        {
            if (_projects.Any())
            {
                var x = _projects.Last();
                item.Id = x.Id + 1;
                _projects.Add(item);
            }
            else
            {
                item.Id = 1;
                _projects.Add(item);
            }
        }

        public Project Get(int id)
        {
            return _projects.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Project> GetAll()
        {
            return _projects;
        }

        public void Delete(int id)
        {
            _projects.RemoveAt(_projects.FindIndex(x => x.Id == id));
        }

        public void Update(Project item)
        {
            var project = _projects.FirstOrDefault(x => x.Id == item.Id);
            if (project != null)
            {
                var index = _projects.FindIndex(x => x.Id == item.Id);
                _projects[index] = item;
            }
        }
    }
}
