﻿using BSA.DataAccess.UnityOfWork;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using AutoMapper;
using BSA.BLL.DTOs;
using BSA.DataAccess.Entities;

namespace BSA.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ProjectsController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> GetAll()
        {
            return Ok(_unitOfWork.Projects.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> Get(int id)
        {
            return Ok(_unitOfWork.Projects.Get(id));
        }

        [HttpPost]
        public IActionResult Post([FromBody] ProjectDTO item)
        {
            _unitOfWork.Projects.Create(_mapper.Map<ProjectDTO, Project>(item));
            return Ok();
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromBody] ProjectDTO item)
        {
            _unitOfWork.Projects.Update(_mapper.Map<ProjectDTO, Project>(item));
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _unitOfWork.Projects.Delete(id);
            return NoContent();
        }
    }
}
