﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using AutoMapper;
using BSA.BLL.DTOs;
using BSA.DataAccess.Entities;
using BSA.DataAccess.UnityOfWork;

namespace BSA.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TasksController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TaskDTO>> GetAll()
        {
            return Ok(_unitOfWork.Tasks.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<TaskDTO> Get(int id)
        {
            return Ok(_unitOfWork.Tasks.Get(id));
        }

        [HttpPost]
        public IActionResult Post([FromBody] TaskDTO item)
        {
            _unitOfWork.Tasks.Create(_mapper.Map<TaskDTO, Task>(item));
            return Ok();
        }

        [HttpPut("{id}")]
        public IActionResult Put([FromBody] TaskDTO item)
        {
            _unitOfWork.Tasks.Update(_mapper.Map<TaskDTO, Task>(item));
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _unitOfWork.Tasks.Delete(id);
            return NoContent();
        }
    }
}
